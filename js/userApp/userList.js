$(function() {
                $('#frmAddUser').validate({
                    errorClass: "help-block",
                    errorElement: "span",
                    errorPlacement: function(error, element) {
                        element.parents(".controls").append(error);
                    },
                    highlight: function(e) {
                        $(e).closest(".control-group").removeClass("success error").addClass("error");
                    },
                    success: function(e) {
                        e.addClass("valid").closest(".control-group").removeClass("success error").addClass("success");
                    },
                    rules: {
                        username: {
                            required: true,
                            minlength: 3,
                            maxlength: 3,
                            
                        },
                        userFullName: {
                            required: true
                        },
                        userType: {
                            required: true
                        },
                        userEmail: {
                            required: true,
                            email: true
                        },
                        userPassword: {
                            required: true,
                            minlength: 6
                        },
                        retypeUserPassword: {
                            required: true,
                            minlength: 6,
                            equalTo: "#userPassword"
                         },   
                       uCaptcha: {
                            required: true,
                            minlength: 5,
                            maxlength: 5,
                            equalTo: "#captcha"     
                        }                   
                        
                    },
                    messages: {
                        username: {
                            required: "Please enter a username must no more than 3 characters",
                            minlength: "Your username must consist of at least 3 characters",
                            maxlength: "Your username must no more than 3 characters"
                            
                        },
                        userFullName:"Please enter a fullname",
                        userEmail: "Please enter a valid email address",
                         userPassword: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        retypeUserPassword: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long",
                            equalTo: "Please enter the same password as above"
                        },  
                        uCaptcha: {
                            required: "Please enter  a captcha",
                            minlength: "Captcha must be at least 5 characters long",
                            maxlength: "Captcha must no more than 5 characters",
                            equalTo: "Please enter the same captcha as above"
                        },                              
                        userType: "Please select a user type"
                        /* divisionCode: "Please select a division",
                        departmentCode:"Please select a department"   */
                        }
                });
                $('#frmEditProfile').validate({
                    errorClass: "help-block",
                    errorElement: "span",
                    errorPlacement: function(error, element) {
                        element.parents(".controls").append(error);
                    },
                    highlight: function(e) {
                        $(e).closest(".control-group").removeClass("success error").addClass("error");
                    },
                    success: function(e) {
                        e.addClass("valid").closest(".control-group").removeClass("success error").addClass("success");
                    },
                    rules: {
                        currentpassword: {
                            required: true,
                            minlength: 6
                        },
                        newpassword: {
                            required: true,
                            minlength: 6
                        },
                        retypenewpassword: {
                            required: true,
                            minlength: 6,
                            equalTo: "#newpassword"
                         }                 
                        
                    },
                    messages: {
                        
                        currentpassword: {
                            required: "Please provide a current password",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        newpassword: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        retypenewpassword: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long",
                            equalTo: "Please enter the same password as above"
                        }
                        }
                });
                
                $('#frmEditUser').validate({
                    errorClass: "help-block",
                    errorElement: "span",
                    errorPlacement: function(error, element) {
                        element.parents(".controls").append(error);
                    },
                    highlight: function(e) {
                        $(e).closest(".control-group").removeClass("success error").addClass("error");
                    },
                    success: function(e) {
                        e.addClass("valid").closest(".control-group").removeClass("success error").addClass("success");
                    },
                    rules: {
                        userFullName: {
                            required: true
                        },
                        directorateCode: {
                            required: true
                        },
                        divisionCode: {
                            required: true
                        },
                        departmentCode: {
                            required: true
                        }, 
                        
                        userEmail: {
                            required: true,
                            email: true
                        }                     
                        
                    },
                    messages: {
                        userFullName:"Please enter a fullname",
                        userEmail: "Please enter a valid email address",
                        directorateCode: "Please select a directorate",
                        divisionCode: "Please select a division",
                        departmentCode:"Please select a department"
                        }
                });
            });