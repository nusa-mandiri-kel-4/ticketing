$(function() {
                $('#frmAddUserApp').validate({
                    errorClass: "help-block",
                    errorElement: "span",
                    errorPlacement: function(error, element) {
                        element.parents(".controls").append(error);
                    },
                    highlight: function(e) {
                        $(e).closest(".control-group").removeClass("success error").addClass("error");
                    },
                    success: function(e) {
                        e.addClass("valid").closest(".control-group").removeClass("success error").addClass("success");
                    },
                    rules: {
                         colorAppCode: {
                            required: true
                        },
                        appName: {
                            required: true,
                            minlength: 6
                        },
                        appUrl: {
                            required: true
                        }

                    },
                    messages: {
                        appName: {
                            required: "Please enter a application name",
                            minlength: "Application name must consist of at least 6 characters"
                        },
                        appUrl: {
                            required: "Please enter a application URL"
                        },
                        colorAppCode: "Please select a color theme!"
                    }
                });
                
                $('#frmEditUserApp').validate({
                    errorClass: "help-block",
                    errorElement: "span",
                    errorPlacement: function(error, element) {
                        element.parents(".controls").append(error);
                    },
                    highlight: function(e) {
                        $(e).closest(".control-group").removeClass("success error").addClass("error");
                    },
                    success: function(e) {
                        e.addClass("valid").closest(".control-group").removeClass("success error").addClass("success");
                    },
                    rules: {
                         colorAppCode: {
                            required: true
                        },
                        appName: {
                            required: true,
                            minlength: 6
                        },
                        appUrl: {
                            required: true
                        }

                    },
                    messages: {
                        appName: {
                            required: "Please enter a application name",
                            minlength: "Application name must consist of at least 6 characters"
                        },
                        appUrl: {
                            required: "Please enter a application URL"
                        },
                        colorAppCode: "Please select a color theme!"
                    }
                });                
            });