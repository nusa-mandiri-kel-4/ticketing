<?php
error_reporting (E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
#HOSTING
$DB_TYPE        = "mysql";
$DB_HOST        = "localhost";
$DB_USER        = "root";
$DB_PASS        = "admin";
$DB_DEFAULT     = "ticketing";
$WEB	        = "ticketing";
$PATH_WEB       = $_SERVER['DOCUMENT_ROOT']."/".$WEB ;
$PATH_FUNCTION	= $PATH_WEB."/function";
$PATH_LIB		= $PATH_WEB."/lib";
$PATH_ADODB		= $PATH_LIB."/adodb";
$PATH_TP		= $PATH_LIB."/tp";
$PATH_TEMPLATE 	= $PATH_WEB."/templates";
$PATH_MODULE 	= $PATH_WEB."/modules";

$DOMAIN			= "http://localhost:85/".$WEB."/";
$DOMAIN_CP		= "http://localhost:85/".$WEB;
$COPYRIGHT 		= "© Copyright 2021. PT Excellent Solusindo";
$TITLE_CP  		= "Secure Control Panel Web Site"; 
$COMPANY		= "PT Equity Life Indonesia";
$DATE			= date('l, j F Y');

$NOW_EXPORT		= date('d-m-Y H:i:s');
$LOGIN_DATE		= date('l, j F Y');
$LOGIN_TIME		= date('H:i');
$URL 			= "/";
$IP_RA			= $_SERVER["REMOTE_ADDR"];
$BROWSER_CLIENT	= $_SERVER["HTTP_USER_AGENT"];
$IP 			= getenv("REMOTE_ADDR");
?>