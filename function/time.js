function startTime()
{
var today=new Date()
var h=today.getHours()
var m=today.getMinutes()
var s=today.getSeconds()

// add a zero in front of numbers<10
h=checkTime(h)
m=checkTime(m)
s=checkTime(s)
document.getElementById('time').innerHTML="&nbsp;"+printDate()+" "+h+":"+m+":"+s
t=setTimeout('startTime()',500)
}

function checkTime(i)
{
if (i<10) 
  {i="0" + i}
  return i
}

function printDate()
{
var today=new Date()
var weekday=new Array(7)
var monthname=new Array(12)

weekday[0]="Sunday"
weekday[1]="Monday"
weekday[2]="Tuesday"
weekday[3]="Wednesday"
weekday[4]="Thursday"
weekday[5]="Friday"
weekday[6]="Saturday"

monthname[0]="January"
monthname[1]="February"
monthname[2]="March"
monthname[3]="April"
monthname[4]="May"
monthname[5]="June"
monthname[6]="July"
monthname[7]="August"
monthname[8]="September"
monthname[9]="October"
monthname[10]="November"
monthname[11]="December"

var day=weekday[today.getDay()]
var date=today.getDate()
var month=monthname[today.getMonth()]
var year=today.getYear()

return day+", "+date+" "+month+" "+year
}