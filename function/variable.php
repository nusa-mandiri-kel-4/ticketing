<?
#TM Level and Target
$TM = array('Level' => array ('Trainee','Associate','Senior'),
		    'TargetInAP' => array (15,20,25),
			'TargetInValue' => array (15000000,20000000,25000000)
  	        );

#TM Commision
$commision = array ('5s/d9' => 0.03,
					'10s/d25' => 0.07,
					'>25' => 0.11
					);
?>