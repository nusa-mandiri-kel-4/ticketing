<?php
function sql_inject(){
	$resarray = array();
	foreach (func_get_args() as $ourvar) {
		if (!get_magic_quotes_runtime()) {
			$ourvar = addslashes($ourvar);
		}
		array_push($resarray, $ourvar);
	}
	if (func_num_args() == 1) {
		return $resarray[0];
	} else {
		return $resarray;
	}
}

function get_file_extension($file_name) {
	return substr(strrchr($file_name,'.'),1);
}

?>