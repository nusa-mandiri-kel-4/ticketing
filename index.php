<?php
  error_reporting (E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	//error_reporting(E_ALL & ~E_NOTICE);
	error_reporting(0);
	require_once('function/config.php');
	require_once('function/function.php');
  require_once($PATH_ADODB.'/adodb.inc.php');
	require_once($PATH_ADODB.'/pivottable.inc.php');
  require_once($PATH_ADODB.'/adodb-errorhandler.inc.php');  
	require_once($PATH_TP.'/class.TemplatePower.inc.php'); 

	$DB = ADONewConnection($DB_TYPE);
	$conn = $DB->Connect($DB_HOST,$DB_USER,$DB_PASS,$DB_DEFAULT);
  $referer = $_SERVER['HTTP_REFERER'];
	$domain = explode("/",$referer);
  session_start();
	$sess_usertypeid=$_SESSION['ses_userTypeId'];

  if($DB ==1)  {
  	$template = "user_index.html";
		$TPL = new TemplatePower("templates/".$template);
		
		if ($_GET['act']=="main") {
				require_once $PATH_MODULE."/session_check.php";  
				$TPL->assignInclude("MAIN", $PATH_TEMPLATE."/user_main.html");
				$TPL->assignInclude("LEFT", $PATH_TEMPLATE."/user_left_".$sess_usertypeid.".html");
				$TPL->assignInclude("CONTENT", $PATH_TEMPLATE."/user_content.html");  
      }
    elseif (($_GET['act']=="user")
         or ($_GET['act']=="adduser") 
         or ($_GET['act']=="sadduser") 
         or ($_GET['act']=="viewuser")
         or ($_GET['act']=="profile")
         or ($_GET['act']=="changepassword")
         or ($_GET['act']=="vieweditapp")                
         or ($_GET['act']=="saddpriviliges")
         or ($_GET['act']=="seditpriviliges") 
         or ($_GET['act']=="delpriviliges")
         or ($_GET['act']=="deluser")
         or ($_GET['act']=="sedituser")) {
				require_once $PATH_MODULE."/session_check.php";
				$TPL->assignInclude("MAIN", $PATH_TEMPLATE."/user_main.html");
				$TPL->assignInclude("LEFT", $PATH_TEMPLATE."/user_left_".$sess_usertypeid.".html");
				$TPL->assignInclude("CONTENT", $PATH_TEMPLATE."/user_user.html");  
      } 

   elseif (($_GET['act']=="category") 
         or ($_GET['act']=="addcategory")
          or ($_GET['act']=="viewcategory")
         or ($_GET['act']=="saddcategory")
         or ($_GET['act']=="editcategory")
         or ($_GET['act']=="seditcategory")
         or ($_GET['act']=="delcategory")) {         
				require_once $PATH_MODULE."/session_check.php";
				$TPL->assignInclude("MAIN", $PATH_TEMPLATE."/user_main.html");
				$TPL->assignInclude("LEFT", $PATH_TEMPLATE."/user_left_".$sess_usertypeid.".html");
				$TPL->assignInclude("CONTENT", $PATH_TEMPLATE."/user_category.html");    
  } 
   elseif (($_GET['act']=="ticket") 
         or ($_GET['act']=="addticket")
          or ($_GET['act']=="viewticket")
         or ($_GET['act']=="saddticket")
         or ($_GET['act']=="editticket")
         or ($_GET['act']=="seditticket")
         or ($_GET['act']=="delticket")) {         
				require_once $PATH_MODULE."/session_check.php";
				$TPL->assignInclude("MAIN", $PATH_TEMPLATE."/user_main.html");
				$TPL->assignInclude("LEFT", $PATH_TEMPLATE."/user_left_".$sess_usertypeid.".html");
				$TPL->assignInclude("CONTENT", $PATH_TEMPLATE."/user_ticket.html");    
  }    
  elseif (($_GET['act']=="report") ) {         
 require_once $PATH_MODULE."/session_check.php";
 $TPL->assignInclude("MAIN", $PATH_TEMPLATE."/user_main.html");
 $TPL->assignInclude("LEFT", $PATH_TEMPLATE."/user_left_".$sess_usertypeid.".html");
 $TPL->assignInclude("CONTENT", $PATH_TEMPLATE."/user_report.html");    
}  
    elseif (($_GET['act']=="tickethistory")
        or ($_GET['act']=="viewtickethistory")) {         
        require_once $PATH_MODULE."/session_check.php";
        $TPL->assignInclude("MAIN", $PATH_TEMPLATE."/user_main.html");
        $TPL->assignInclude("LEFT", $PATH_TEMPLATE."/user_left_".$sess_usertypeid.".html");
        $TPL->assignInclude("CONTENT", $PATH_TEMPLATE."/user_ticket_history.html");    
      } 

     else {
				$TPL->assignInclude("MAIN", $PATH_TEMPLATE."/user_login.html");
		}
	}
  else{
		$template =  "user_error.html";
	}
	
	#making template(s)
	$TPL->prepare();
	

  if (($_GET['act']=="login")) { 
		  require_once $PATH_MODULE."/user_check.php"; 
         
  }
	elseif (($_GET['act']=="logout")) { 
    session_destroy();
    header("location: ".$DOMAIN);
		  require_once $PATH_MODULE."/user_check.php"; 
  }
  elseif ($_GET['act']=="main") {         
      require_once $PATH_MODULE."/user_check.php";
		  require_once $PATH_MODULE."/user_main.php"; 
        
  }
  
  elseif (($_GET['act']=="user")
         or ($_GET['act']=="adduser")
         or ($_GET['act']=="sadduser")   
         or ($_GET['act']=="viewuser")
         or ($_GET['act']=="profile")
         or ($_GET['act']=="changepassword")
         or ($_GET['act']=="vieweditapp")
         or ($_GET['act']=="saddpriviliges")
         or ($_GET['act']=="seditpriviliges") 
         or ($_GET['act']=="delpriviliges")
         or ($_GET['act']=="deluser")
         or ($_GET['act']=="sedituser")) {         
		  require_once $PATH_MODULE."/user_check.php";
      require_once $PATH_MODULE."/user_user.php"; 
           
  }
 
     elseif (($_GET['act']=="category") 
         or ($_GET['act']=="addcategory")
          or ($_GET['act']=="viewcategory")
         or ($_GET['act']=="saddcategory")
         or ($_GET['act']=="editcategory")
         or ($_GET['act']=="seditcategory")
         or ($_GET['act']=="delcategory")) {         
		  require_once $PATH_MODULE."/user_check.php";
      require_once $PATH_MODULE."/user_category.php";   
  } 
     elseif (($_GET['act']=="ticket") 
         or ($_GET['act']=="addticket")
          or ($_GET['act']=="viewticket")
         or ($_GET['act']=="saddticket")
         or ($_GET['act']=="editticket")
         or ($_GET['act']=="seditticket")
         or ($_GET['act']=="delticket")) {         
		  require_once $PATH_MODULE."/user_check.php";
      require_once $PATH_MODULE."/holiday.php";
      require_once $PATH_MODULE."/user_ticket.php";  
    
  }      
  elseif (($_GET['act']=="tickethistory") 
  or ($_GET['act']=="viewtickethistory")) {         
require_once $PATH_MODULE."/user_check.php";
require_once $PATH_MODULE."/holiday.php";
require_once $PATH_MODULE."/user_ticket_history.php";  

}  
elseif (($_GET['act']=="report") ) {         
require_once $PATH_MODULE."/user_check.php";
require_once $PATH_MODULE."/holiday.php";
require_once $PATH_MODULE."/user_report.php";  

}           
  else {
   require_once $PATH_MODULE."/user_check.php";
  } 
	$TPL->assignGlobal("DATE",date('d M Y'));
	$TPL->assignGlobal("USERNAME",$_SESSION['ses_userName']);
	$TPL->assignGlobal("USERFULLNAME",$_SESSION['ses_userFullName']);
  $TPL->assignGlobal("IP",$IP);
  
	$TPL->printToScreen();
	$DB->Close(); 
?>
