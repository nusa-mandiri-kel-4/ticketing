<?php
	if ($_GET['act']=="adduser") {
	   $userName=$_GET['userName'];
    $TPL->newBlock("CURRENTLI");
    $TPL->assign("CURRENTSETTING","active"); 
    $TPL->assign("CURRENTSETTINGUSER","active");
      
    $TPL->newBlock("HEADER");
    $TPL->assign("HEADERTITLE","Create User");
    $TPL->assign("HEADERDESC","Create user Account"); 
    $TPL->assign("HEADERICON","glyphicon-user"); 
    
    $TPL->newBlock("BREADCRUMB");
    $TPL->assign("BREADCRUMBICON","glyphicon-display");
    $TPL->assign("BREADCRUMBLEVEL1","Setting"); 
    $TPL->assign("BREADCRUMBLINKLEVEL1","?act=main"); 
    $TPL->assign("BREADCRUMBLEVEL2","User");     
    $TPL->assign("BREADCRUMBACTIVELEVEL2","");
    $TPL->assign("BREADCRUMBLINKLEVEL2","?act=user"); 
    $TPL->assign("BREADCRUMBRIGHTLEVEL2","icon-angle-right"); 
    $TPL->assign("BREADCRUMBLEVEL3","Create");     
    $TPL->assign("BREADCRUMBACTIVELEVEL3","active");
    $TPL->newBlock("ADD");

        $SQL1 = "SELECT userTypeId,userTypeName FROM m_user_type where rowstatus='1'  ORDER BY userTypeName ASC";		
        $RS1 = $DB->Execute($SQL1);
        if($RS1 AND !$RS1->EOF) {
        $usertype="<option value='' selected>Option select</option>";
        while(!$RS1->EOF) {
            $usertype = $usertype."<option value=".$RS1->fields['userTypeId'].">".$RS1->fields['userTypeName']."</option>";
            $RS1->MoveNext();	
        }
        $RS1->Close();
      }
      $TPL->assign("USERTYPE",$usertype);
         
          
  } 
	elseif ($_GET['act']=="sadduser") { 
     
	   $user=$_SESSION['ses_userName'];
	   $username=strtoupper($_POST['username']);
     $userFullName = $_POST['userFullName'];
     $userEmail = $_POST['userEmail'];
     $userType = $_POST['userType'];
     $userPassword = $_POST['userPassword'];
          
    $SQL="select username from ".$DB_DEFAULT.".m_user where username='$username'";
    $RS = $DB->Execute($SQL);
    if($RS AND !$RS->EOF) {
    $usernameCheck=$RS->fields['username'];
    $RS->Close();
    } 
    
    if ($usernameCheck==$username){
    echo "<script>alert('Username code $username exist!');location.href='?act=adduser';</script>";
    }
    else { 
    $SQL = "INSERT INTO ".$DB_DEFAULT.".m_user (username,userFullName,password,email,userTypeId,createdBy,createdDate,modifiedBy,modifiedDate,RowStatus) VALUES
		       ('$username','$userFullName','".md5($userPassword)."','$userEmail','$userType','$user',now(),'$user',now(),'1')";
    if ($DB->Execute($SQL)==0) 
    {
			echo "<script>alert('Save Failed !');location.href='?act=adduser';</script>";
		} 
    else 
    {     
    echo "<script>location.href='?act=viewuser&userName=$username';</script>";
    
    }	
	 } 
  } 
  	elseif ($_GET['act']=='userconfirm') {
		
    $id = $_GET['id'];	
		$SQL = "UPDATE m_user SET rowstatus='1' WHERE confirmid='$id'";
		$DB->Execute($SQL);
 		
    $SQL = "select userFullName from m_user WHERE confirmid='$id' and rowstatus='1'";
    $RS = $DB->Execute($SQL); 
    if($RS AND !$RS->EOF) { 
        $TPL->newblock("CONFIRM");
        $TPL->assign("USERFULLNAME",$RS->fields['userFullName']);
        $TPL->assign("MESSAGE","Confirmation successfully, Please use username and password to access application"); 
    }                
	
  }
  
  	elseif ($_GET['act']=="changepassword") {
	   $user=$_SESSION['ses_userName'];
	   $currentpassword=$_POST['currentpassword'];
     $newpassword=md5($_POST['newpassword']);

     $SQL = "select password from m_user WHERE username='$user'";
     $RS = $DB->Execute($SQL); 
     if($RS AND !$RS->EOF) { 
         $password=$RS->fields['password'];
      } 
    if ($password!=md5($currentpassword))
    {
      echo "<script>alert('Current password invalid.');location.href='?act=profile';</script>";
    }
    else
    {
      $SQL = "UPDATE m_user SET password='$newpassword',modifiedBy='$user',modifiedDate=now() WHERE username='$user'";
      if ($DB->Execute($SQL)==0) {
        echo "<script>alert('Update Failed !');location.href='?act=profile';</script>";
      } else
        { 
          echo "<script>alert('Change password successfully.');location.href='?act=profile';</script>";
        }
    }
		  
    

   
  } 
  	elseif ($_GET['act']=="viewuser") {
    $userName=$_GET['userName'];
    $TPL->newBlock("CURRENTLI");
    $TPL->assign("CURRENTSETTING","active"); 
    $TPL->assign("CURRENTSETTINGUSER","active");
      
    $TPL->newBlock("HEADER");
    $TPL->assign("HEADERTITLE","User");
    $TPL->assign("HEADERDESC","View detail user"); 
    $TPL->assign("HEADERICON","glyphicon-user"); 
    
    $TPL->newBlock("BREADCRUMB");
    $TPL->assign("BREADCRUMBICON","glyphicon-display");
    $TPL->assign("BREADCRUMBLEVEL1","Setting"); 
    $TPL->assign("BREADCRUMBLINKLEVEL1","?act=main"); 
    $TPL->assign("BREADCRUMBLEVEL2","User");     
    $TPL->assign("BREADCRUMBACTIVELEVEL2","");
    $TPL->assign("BREADCRUMBLINKLEVEL2","?act=user"); 
    $TPL->assign("BREADCRUMBRIGHTLEVEL2","icon-angle-right"); 
    $TPL->assign("BREADCRUMBLEVEL3","View detail");     
    $TPL->assign("BREADCRUMBACTIVELEVEL3","active");    
     
    $SQL = "SELECT 
       u.userId
      ,u.userName
      ,u.userFullName
      ,u.email
      ,u.createdBy
      ,u.createdDate
      ,u.modifiedBy
      ,u.modifiedDate
      ,u.rowstatus
      FROM ".$DB_DEFAULT.".m_User u 
      where u.userName='$userName'";
      //u.rowstatus='1' and 
        $RS = $DB->Execute($SQL);    	
        if($RS AND !$RS->EOF) { 
        $TPL->newblock("VIEW");
        $TPL->assign("USERNAME",$RS->fields['userName']);
       	$TPL->assign("USERFULLNAME",$RS->fields['userFullName']);
        $TPL->assign("USEREMAIL",$RS->fields['email']);
      	if (($RS->fields['rowstatus'])=='1'){
        $status="Active";			
				}
				else{ 
        $status="In-Active";			
				}
        $TPL->assign("STATUS",$status); 
  }
}
	elseif ($_GET['act']=="profile") {
    $user=$_SESSION['ses_userName'];
    $TPL->newBlock("CURRENTLI");
    $TPL->assign("CURRENTPROFILE","active"); 

    $TPL->newBlock("HEADER");
    $TPL->assign("HEADERTITLE","Profile");
    $TPL->assign("HEADERDESC","Biodata & Security"); 
    $TPL->assign("HEADERICON","glyphicon-user"); 
    
    $TPL->newBlock("BREADCRUMB");
    $TPL->assign("BREADCRUMBICON","glyphicon-display");
    $TPL->assign("BREADCRUMBLEVEL1","Profile"); 
    $TPL->assign("BREADCRUMBLINKLEVEL1","?act=main"); 
    $TPL->assign("BREADCRUMBLEVEL2","Biodata");     
     
    $SQL = "SELECT 
       u.userId
      ,u.userName
      ,u.userFullName
      ,u.email
      ,u.createdBy
      ,u.createdDate
      ,u.modifiedBy
      ,u.modifiedDate
      ,u.rowstatus
      FROM ".$DB_DEFAULT.".m_User u 
      where u.userName='$user'";
        $RS = $DB->Execute($SQL); 
        if($RS AND !$RS->EOF) { 
        $TPL->newblock("PROFILE");
        $TPL->assign("USERNAME",$RS->fields['userName']);
       	$TPL->assign("USERFULLNAME",$RS->fields['userFullName']);
        $TPL->assign("USEREMAIL",$RS->fields['email']);
      	if (($RS->fields['rowstatus'])=='1'){
        $status="Active";			
				}
				else{ 
        $status="In-Active";			
				}
        $TPL->assign("STATUS",$status); 
  }	
		
 } 

   elseif ($_GET['act']=="user") {
    $user=$_SESSION['ses_userName'];
    $TPL->newBlock("CURRENTLI");
    $TPL->assign("CURRENTSETTING","active"); 
    $TPL->assign("CURRENTSETTINGUSER","active");
    
    $TPL->newBlock("HEADER");
    $TPL->assign("HEADERTITLE","User");
    $TPL->assign("HEADERDESC","User list"); 
    $TPL->assign("HEADERICON","glyphicon-user"); 
    
    $TPL->newBlock("BREADCRUMB");
    $TPL->assign("BREADCRUMBICON","glyphicon-display");
    $TPL->assign("BREADCRUMBLEVEL1","Setting"); 
    $TPL->assign("BREADCRUMBLINKLEVEL1","?act=main"); 
    $TPL->assign("BREADCRUMBLEVEL2","User");     
    $TPL->assign("BREADCRUMBACTIVELEVEL2","active");
    $TPL->assign("BREADCRUMBLINKLEVEL2","?act=user"); 
    $TPL->newBlock("INQUIRY");

     if (isset($_POST['userFullName']))
      $userFullName = $_POST['userFullName'];
		else
      $userFullName = $_GET['userFullName'];

    if (isset($_POST['userEmail']))
      $userEmail = $_POST['userEmail'];
		else
      $userEmail = $_GET['userEmail'];
    
		if(($userFullName=='') and ($userEmail==''))
		{
		$filter="";
		} 
    else
    {
    if ($userFullName=='')
			$filter = "WHERE 1=1 ";	
		elseif ($userFullName!='') 
		  $filter = "WHERE u.userFullName like '%$userFullName%'";

    if ($userEmail=='')
			$filter = $filter;	
		elseif ($userEmail!='') 
		  $filter = $filter."and u.email like '%$userEmail%'";
    }	
    
      $SQL = "SELECT u.userId
      ,u.userName
      ,u.userFullName
      ,u.email
      ,u.createdBy
      ,u.createdDate
      ,u.modifiedBy
      ,u.modifiedDate
      ,u.rowStatus
      ,ut.userTypeName
      FROM ".$DB_DEFAULT.".m_user u 
      inner join m_user_type ut ON u.userTypeId=ut.userTypeId
      ".$filter." order by u.createdDate desc";
	    
    $TPL->newBlock("TABLE");
		$RS1 = $DB->Execute($SQL);
		if($RS1 AND !$RS1->EOF) {
		  $no=0;
			while(!$RS1->EOF) {
			  $no++;
			  
				$TPL->newBlock("LIST");
				$TPL->assign("NO",$no);
				$TPL->assign("USERNAME",$RS1->fields['userName']);
				$TPL->assign("USERFULLNAME",$RS1->fields['userFullName']); 
        $TPL->assign("EMAIL",$RS1->fields['email']); 
        $TPL->assign("USERTYPE",$RS1->fields['userTypeName']);               			
				$TPL->assign("CREATEDBY",$RS1->fields['createdBy']);
        $TPL->assign("CREATEDDATE",$RS1->fields['createdDate']);
				$TPL->assign("MODIFIEDBY",$RS1->fields['modifiedBy']);
				$TPL->assign("MODIFIEDDATE",date('d M Y', strtotime($RS1->fields['modifiedDate'])));
				if ($RS1->fields['rowStatus']=='1'){
        $TPL->assign("STATUS","Active");
        } else
        {
        $TPL->assign("STATUS","In-Active");}
        
				$RS1->MoveNext();	
			}
		$RS1->Close();
		} 
  	$TPL->newBlock("PAGE");
		$TPL->assign("PAGE",$strpage);
		$TPL->assign("PREV",$prev);
		$TPL->assign("NEXT",$next);     		
	}


  elseif ($_GET['act']=="saddpriviliges") {
    $username=$_POST['username'];
    $appCode=$_POST['appCode']; 
		$user=$_SESSION['ses_userName'];
       
    $SQL="SELECT ua.appCode,a.appName FROM m_userApp ua inner join m_app a ON ua.appCode=a.appCode where ua.appCode='$appCode' and ua.username='$username'";
    $RS = $DB->Execute($SQL);
    if($RS AND !$RS->EOF) {
    $appCodeCheck=$RS->fields['appCode'];
    $appNameCheck=$RS->fields['appName'];
    $RS->Close();
    } 
          
    if ($appCode==$appCodeCheck){
    echo "<script>alert('App Name $appNameCheck exist!');window.history.back();</script>";         
    }
    else { 
    $SQL = "INSERT INTO m_userApp (username,appCode,rowstatus,createdBy,createdDate,modifiedBy,modifiedDate) VALUES
		       ('$username','$appCode','1','$user',GETDATE(),'$user',GETDATE())";
    if ($DB->Execute($SQL)==0) {
			echo "<script>alert('Save Failed !');window.history.back();</script>";
		} else
			echo "<script>window.history.back();</script>";	
	}
 }
 
 
  	elseif ($_GET['act']=="deluser") {
		$username=$_GET['username'];
    $user=$_SESSION['ses_userName'];
		$SQL = "UPDATE ".$DB_DEFAULT.".m_user SET rowstatus='0',modifiedBy='$user',modifiedDate=now() WHERE username='$username' and rowstatus='1'";
	
		if ($DB->Execute($SQL)==0) {
			echo "<script>alert('Delete Failed !');location.href='?do=user';</script>";
		} else
			echo "<script>location.href='?act=user';</script>";
	}
 
   elseif ($_GET['act']=="sedituser") {
    $userName=$_GET['userName'];
    $userFullName=$_POST['userFullName'];
    $userEmail=$_POST['userEmail'];
    $directorateCode=$_POST['directorateCode'];
    $divisionCode=$_POST['divisionCode'];
    $departmentCode=$_POST['departmentCode'];
		$user=$_SESSION['ses_userName'];


    $SQL = "Update m_user set
            userFullName='$userFullName',
            userEmail='$userEmail',
            directorateCode='$directorateCode',
            divisionCode='$divisionCode',
            departmentCode='$departmentCode',
            modifiedBy='$user',
            modifiedDate=now() 
            where userName='$userName'";
    if ($DB->Execute($SQL)==0) {
			echo "<script>alert('Update Failed !');window.history.back();</script>";
		} else
			echo "<script>window.history.back();</script>";	
	}        
    ?>