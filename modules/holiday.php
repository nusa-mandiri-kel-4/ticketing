<?php
function number_of_working_dates($from, $days) {
    global $slaFunction;	
							$workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
							$staticHolidayDays = ['*-01-01','*-12-25','*-08-17']; # variable and fixed holidays
							$from = new DateTime($from);
							$dates = [];
							$dates[] = $from->format('Y-m-d');   
							while ($days) {
								$from->modify('+1 day');
								if (!in_array($from->format('N'), $workingDays)) continue;
								if (in_array($from->format('*-m-d'), $staticHolidayDays)) continue;
								$dates[] = $from->format('Y-m-d');
								$days--;  
							}   
						return $dates[$slaFunction];						 
}
?>
