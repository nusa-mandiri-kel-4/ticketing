<?php 
	if ($_GET['act']=="addticket") {
	 $TPL->newBlock("CURRENTLI");
    $TPL->assign("CURRENTAPPLICATION","active");
      
    $TPL->newBlock("HEADER");
    $TPL->assign("HEADERTITLE","Request");
    $TPL->assign("HEADERDESC","New Ticket"); 
    $TPL->assign("HEADERICON","glyphicon-folder_plus"); 
    
    $TPL->newBlock("BREADCRUMB");
    $TPL->assign("BREADCRUMBICON","glyphicon-display");
    $TPL->assign("BREADCRUMBLEVEL1","Ticket"); 
    $TPL->assign("BREADCRUMBLINKLEVEL1","?act=main"); 
    $TPL->assign("BREADCRUMBLEVEL2","Ticket List");        
    $TPL->assign("BREADCRUMBACTIVELEVEL2","");
    $TPL->assign("BREADCRUMBLINKLEVEL2","?act=ticket"); 
    $TPL->assign("BREADCRUMBRIGHTLEVEL2","icon-angle-right"); 
    $TPL->assign("BREADCRUMBLEVEL3","New Ticket");     
    $TPL->assign("BREADCRUMBACTIVELEVEL3","active"); 
    
    
    $TPL->newBlock("ADD");
    $SQL1 = "SELECT categoryId,categoryCode,categoryName FROM m_category where rowstatus='1'  ORDER BY categoryName ASC";		
    $RS1 = $DB->Execute($SQL1);
    if($RS1 AND !$RS1->EOF) {
    $category="<option value='' selected>Option select</option>";
    while(!$RS1->EOF) {
        $category = $category."<option value=".$RS1->fields['categoryCode'].">".$RS1->fields['categoryName']."</option>";
        $RS1->MoveNext();	
    }
    $RS1->Close();
  }
  $TPL->assign("CATEGORY",$category);

  		
	}
	elseif ($_GET['act']=="saddticket") {
    if(isset($_POST['draftbtn'])){
      $status='S07';
    }
    else
    {
      $status='S01';
    }
  $user=$_SESSION['ses_userName'];
  $title=$_POST['title'];
  $description=$_POST['description'];
  $category=$_POST['category'];
    $today = date("ymd");
    $addchar='T';
    $query = "SELECT max(right(ticketNo,3)) AS last FROM ".$DB_DEFAULT.".ticket where ticketNo LIKE '%$today%'";
    $RS = $DB->Execute($query);      	
    if($RS AND !$RS->EOF) 
    {  
    $lastCode=$RS->fields['last'];
    $nextNoUrut = $lastCode + 1;
    $appCode= $addchar.$today.sprintf('%03s', $nextNoUrut);
    } 
    $date=date('d M Y h:i:s A');
    $userFullName=$_SESSION['ses_userFullName'];
    $finaldescription=$userFullName." request on ".$date."&#013;================================================&#013;".$description;

    $target_dir = "uploads/";
        mkdir($target_dir."/".$appCode, 0777, true);
        $target_file = $target_dir .$appCode."/". basename($_FILES["file"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
          $msg= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
          $uploadOk = 0;
        }
       
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
          $msg= "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
          if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
            $msg="The file ". htmlspecialchars( basename( $_FILES["file"]["name"])). " has been uploaded.";
          } else {
            $msg="Sorry, there was an error uploading your file.";
          }
        }

    $SQL = "INSERT INTO ".$DB_DEFAULT.".ticket (ticketNo,title,description,status,categoryCode,requestDate,createdBy,createdDate,modifiedBy,modifiedDate,RowStatus,filename) VALUES
		       ('$appCode','$title','$finaldescription','$status','$category',now(),'$user',now(),'$user',now(),'1','$target_file')";
    if ($DB->Execute($SQL)==0) {
			echo "<script>alert('Save Failed !');location.href='?act=addticket';</script>";
		} else
			echo "<script>location.href='?act=ticket';</script>";
	}
elseif ($_GET['act']=="viewticket") {
    $ticketno=$_GET['ticketNo'];
    $TPL->newBlock("CURRENTLI");
    $TPL->assign("CURRENTTICKET","active");
    $TPL->assign("CURRENTTICKETLIST","active");
      
    $TPL->newBlock("HEADER");
    $TPL->assign("HEADERTITLE","Detail");
    $TPL->assign("HEADERDESC","View detail ticket"); 
    $TPL->assign("HEADERICON","glyphicon-settings"); 
    
    $TPL->newBlock("BREADCRUMB");
    $TPL->assign("BREADCRUMBICON","glyphicon-display");
    $TPL->assign("BREADCRUMBLEVEL1","Master Appilcation"); 
    $TPL->assign("BREADCRUMBLINKLEVEL1","?act=main"); 
    $TPL->assign("BREADCRUMBLEVEL2","Application");     
    $TPL->assign("BREADCRUMBACTIVELEVEL2","");
    $TPL->assign("BREADCRUMBLINKLEVEL2","?act=ticket"); 
    $TPL->assign("BREADCRUMBRIGHTLEVEL2","icon-angle-right"); 
    $TPL->assign("BREADCRUMBLEVEL3","View detail");     
    $TPL->assign("BREADCRUMBACTIVELEVEL3","active");    
     
    $SQL = "SELECT t.ticketId, t.ticketNo, t.title, t.description, t.file, t.createdDate, t.createdBy, t.modifiedBy, t.modifiedDate, 
      t.rowStatus, t.status, u.userFullName,st.statusName,t.categoryCode,c.categoryName,t.requestDate,t.closedDate,t.completedDate,t.filename
      FROM ".$DB_DEFAULT.".ticket t 
      inner join m_status st ON t.status=st.statuscode
      inner join m_category c ON t.categoryCode=c.categoryCode
      LEFT JOIN ".$DB_DEFAULT.".m_user u ON t.createdBy=u.username
      where t.ticketNo='$ticketno' order by t.ticketNo Desc";
    
		$RS = $DB->Execute($SQL);
		if($RS AND !$RS->EOF) {
				$TPL->newBlock("VIEWTICKET");
				$TPL->assign("TICKETID",$RS->fields['ticketId']);
				$TPL->assign("TICKETNO",$RS->fields['ticketNo']);
				$TPL->assign("TITLE",$RS->fields['title']);
        $TPL->assign("DESCRIPTION",$RS->fields['description']);
				$TPL->assign("FILE",$RS->fields['file']);
				$TPL->assign("STATUS",$RS->fields['status']);
        $TPL->assign("CREATEDBY",$RS->fields['userFullName']);
        $TPL->assign("CREATEDDATE",$RS->fields['createdDate']);
				$TPL->assign("MODIFIEDBY",$RS->fields['modifiedBy']);
				$TPL->assign("MODIFIEDDATE",$RS->fields['modifiedDate']);                                           
				$TPL->assign("STATUSNAME",$RS->fields['statusName']);
				$TPL->assign("CATEGORYCODE",$RS->fields['categoryCode']);
        $TPL->assign("CATEGORYNAME",$RS->fields['categoryName']);
        $TPL->assign("REQUESTDATE",date('d M Y h:i:s A', strtotime($RS->fields['requestDate'])));
        $TPL->assign("CLOSEDDATE",date('d M Y h:i:s A', strtotime($RS->fields['closedDate'])));
        $TPL->assign("COMPLETEDDATE",date('d M Y h:i:s A', strtotime($RS->fields['completedDate'])));
        if ($RS->fields['filename']<>'')
        {
          $TPL->assign("FILENAME","<a href='".$RS->fields['filename']."' target='_blank'><submit class='btn btn-info'>Download <i class='icon-download'></i></submit></a>");
              
        }
        else
        {
          $TPL->assign("FILENAME","<a href='#'>Document not found</a>");
        }
        
        if ($sess_usertypeid=="3"){
          $condition= "and ms.nextStatusCode not in ('S02')";
        }
        else
        {
          $condition="";
        }

          $statuscode=$RS->fields['status'];
          $SQL1 = "SELECT ms.nextStatusCode,s.statusName FROM m_mapping_status ms 
          inner join m_status s on ms.nextStatusCode=s.statusCode where ms.rowStatus='1' ".$condition." and ms.statusCode='$statuscode' order by s.statusName asc";		
          $RS1 = $DB->Execute($SQL1);
          if($RS1 AND !$RS1->EOF) {
          $statusmapp = $statusmapp."<option value=''>Please select</option>";
          $statusmapp= "<option value=".$RS->fields['status']." selected>".$RS->fields['statusName']."</option>";
          while(!$RS1->EOF) {
               $statusmapp = $statusmapp."<option value=".$RS1->fields['nextStatusCode'].">".$RS1->fields['statusName']."</option>";
               $RS1->MoveNext();	
           }
           $RS1->Close();
         }

     $TPL->assign("STATUSMAPP",$statusmapp);

     $SQL2 = "SELECT username,userFullName FROM m_user
     where rowStatus='1' and userTypeId='2' order by userFullName asc";		
     $RS2 = $DB->Execute($SQL2);
     if($RS2 AND !$RS2->EOF) {
     $pelaksana = $pelaksana."<option value=''>Please select</option>";
     while(!$RS2->EOF) {
          $pelaksana = $pelaksana."<option value=".$RS2->fields['username'].">".$RS2->fields['userFullName']."</option>";
          $RS2->MoveNext();	
      }
      $RS2->Close();
    }
    if ($sess_usertypeid=="1"){
      $TPL->newBlock("ASSIGN"); 
    }
    $TPL->assign("PELAKSANA",$pelaksana);

		$RS->Close();
		}
   

		
 } 

   elseif ($_GET['act']=="ticket") {
    $user=$_SESSION['ses_userName'];
    $TPL->newBlock("CURRENTLI");
    $TPL->assign("CURRENTTICKET","active");
    $TPL->assign("CURRENTTICKETLIST","active");
    
    $TPL->newBlock("HEADER");
    $TPL->assign("HEADERTITLE","Ticket");
    $TPL->assign("HEADERDESC","List"); 
    $TPL->assign("HEADERICON","glyphicon-folder_plus"); 
    
    $TPL->newBlock("BREADCRUMB");
    $TPL->assign("BREADCRUMBICON","glyphicon-display");
    $TPL->assign("BREADCRUMBLEVEL1","Ticket"); 
    $TPL->assign("BREADCRUMBLINKLEVEL1","?act=main"); 
    $TPL->assign("BREADCRUMBLEVEL2","Ticket List");     
    $TPL->assign("BREADCRUMBACTIVELEVEL2","active");
    $TPL->assign("BREADCRUMBLINKLEVEL2","?act=ticket"); 
    $TPL->newBlock("TABLE");
    if ($sess_usertypeid=="1"){
      $condition= "and t.status in ('S01','S05')";
    //  $TPL->newBlock("BTNCREATE");
    }
    elseif ($sess_usertypeid=="2")
    {
      $condition= "and t.status in ('S02') and t.analyst='".$user."'";
    }
    elseif ($sess_usertypeid=="3")
    {
      $condition= "and t.status in ('S01','S02','S03','S05','S07') and t.createdBy='".$user."'";
      $TPL->newBlock("BTNCREATE");
    }
    else
    {
      $condition='';
    }
      $SQL = "SELECT t.ticketId, t.ticketNo, t.title, t.description, t.file, t.createdDate, t.createdBy, t.modifiedBy, t.modifiedDate, 
      t.rowStatus, t.status, u.userFullName,st.statusName,c.categoryName,t.requestDate,t.closedDate,t.completedDate,t.dueDate
      FROM ".$DB_DEFAULT.".ticket t 
      inner join m_status st ON t.status=st.statuscode
      inner join m_category c ON t.categoryCode=c.categoryCode
      LEFT JOIN ".$DB_DEFAULT.".m_user u ON t.createdBy=u.username 
      where t.rowstatus='1' ".$condition." order by t.ticketNo Desc";
    
		$RS = $DB->Execute($SQL);
		if($RS AND !$RS->EOF) {
		  $no=0;
			while(!$RS->EOF) {
			  $no++;
				$TPL->newBlock("LIST");
				$TPL->assign("NO",$no);
				$TPL->assign("TICKETID",$RS->fields['ticketId']);
				$TPL->assign("TICKETNO",$RS->fields['ticketNo']);
				$TPL->assign("TITLE",$RS->fields['title']);
        $TPL->assign("DESCRIPTION",$RS->fields['description']);
				$TPL->assign("FILE",$RS->fields['file']);
				$TPL->assign("STATUS",$RS->fields['status']);
        $TPL->assign("CREATEDBY",$RS->fields['userFullName']);
        $TPL->assign("CREATEDDATE",$RS->fields['createdDate']);
				$TPL->assign("MODIFIEDBY",$RS->fields['modifiedBy']);
				$TPL->assign("MODIFIEDDATE",$RS->fields['modifiedDate']);                                           
				$TPL->assign("STATUS",$RS->fields['statusName']);
				$TPL->assign("CATEGORYNAME",$RS->fields['categoryName']);
        $TPL->assign("REQUESTDATE",date('d M Y', strtotime($RS->fields['requestDate'])));
        $TPL->assign("CLOSEDDATE",date('d M Y h:i:s A', strtotime($RS->fields['closedDate'])));
        $TPL->assign("COMPLETEDDATE",date('d M Y h:i:s A', strtotime($RS->fields['completedDate'])));
        $TPL->assign("DUEDATE",date('d M Y', strtotime($RS->fields['dueDate'])));
				$RS->MoveNext();	
			}
		$RS->Close();
		}    		
	}
 
 
   elseif ($_GET['act']=="seditticket") {
    $ticketno=$_GET['ticketNo'];
    $categorycode=$_GET['categoryCode'];
    $statuscode=$_GET['statusCode'];
    $pelaksana=$_POST['pelaksana'];
    $status=$_POST['status'];
    $readdescription=$_POST['readdescription'];
    $lastdescription=$_POST['lastdescription'];
    $description=$_POST['description'];

    $datesla=date('Y-m-d');
    $slaFunction='1';
    if (($status=="S02") and ($statuscode<>'S02'))
    {
      $SQL1 = "SELECT sla FROM m_category where categoryCode='$categorycode' and rowstatus='1'";		
      $RS1 = $DB->Execute($SQL1);
      if($RS1 AND !$RS1->EOF) {
          $slaFunction = $RS1->fields['sla'];
      $RS1->Close();
      }
      $duedate = print_r(number_of_working_dates($datesla, $slaFunction),true);
      $var="dueDate='$duedate', analyst='$pelaksana',";
    }
    elseif (($status=="S03") and ($statuscode<>'S03'))
    {
      $var="closedDate=now() ,"; 
    }
    elseif (($status=="S04") and ($statuscode<>'S04'))
    {
      $var="completedDate=now(),"; 
    }
    else
    {
      $var='';
    }

    $date=date('d M Y h:i:s A');
    $userFullName=$_SESSION['ses_userFullName'];
    $finaldescription=$userFullName." comment on ".$date."&#013;================================================&#013;".$description."&#013; &#013;".$lastdescription;
		$user=$_SESSION['ses_userName'];
    $SQL = "Update ticket set
            status='$status',
            description='$finaldescription',
            $var
            modifiedBy='$user',
            modifiedDate=now() 
            where ticketNo='$ticketno'";
    if ($DB->Execute($SQL)==0) {
			echo "<script>alert('Update Failed !');location.href='?act=ticket';</script>";
		} else
			echo "<script>location.href='?act=ticket';</script>";	
	}        
    ?>