<?php 
	if ($_GET['act']=="addcategory") {
	 $TPL->newBlock("CURRENTLI");
    $TPL->assign("CURRENTSETTING","active");
    $TPL->assign("CURRENTSETTINGCATEGORY","active");

    $TPL->newBlock("HEADER");
    $TPL->assign("HEADERTITLE","Category");
    $TPL->assign("HEADERDESC","Create Category Name"); 
    $TPL->assign("HEADERICON","glyphicon-file"); 
    
    $TPL->newBlock("BREADCRUMB");
    $TPL->assign("BREADCRUMBICON","glyphicon-display");
    $TPL->assign("BREADCRUMBLEVEL1","Setting"); 
    $TPL->assign("BREADCRUMBLINKLEVEL1","?act=main"); 
    $TPL->assign("BREADCRUMBLEVEL2","Category");        
    $TPL->assign("BREADCRUMBACTIVELEVEL2","");
    $TPL->assign("BREADCRUMBLINKLEVEL2","?act=category"); 
    $TPL->assign("BREADCRUMBRIGHTLEVEL2","icon-angle-right"); 
    $TPL->assign("BREADCRUMBLEVEL3","Create");     
    $TPL->assign("BREADCRUMBACTIVELEVEL3","active"); 
    
    
    $TPL->newBlock("ADD");
 		
	}
	elseif ($_GET['act']=="saddcategory") {
  $user=$_SESSION['ses_userName'];
  $categoryName=$_POST['categoryName'];
  $sla=$_POST['sla'];

    $addchar='C';
    $query = "SELECT max(right(categoryCode,2)) AS last FROM m_category";
    $RS = $DB->Execute($query);      	
    if($RS AND !$RS->EOF) 
    {  
    $lastCode=$RS->fields['last'];
    $nextNoUrut = $lastCode + 1;
    $categoryCode= $addchar.$today.sprintf('%02s', $nextNoUrut);
    } 
    
  $SQL="select categoryName from m_category where categoryName='$categoryName'";
    $RS = $DB->Execute($SQL);
    if($RS AND !$RS->EOF) {
    $categoryNameCheck=$RS->fields['categoryName'];
    $RS->Close();
    } 
          
    if ($categoryNameCheck==$categoryName){
    echo "<script>alert('Category Name $categoryName exist!');location.href='?act=addcategory';</script>";
    }
    else { 
    $SQL = "INSERT INTO m_category (categoryCode,categoryName,sla,createdBy,createdDate,modifiedBy,modifiedDate,RowStatus) VALUES
		       ('$categoryCode','$categoryName','$sla','$user', now(),'$user',now(),'1')";
    if ($DB->Execute($SQL)==0) {
			echo "<script>alert('Save Failed !');location.href='?act=addcategory';</script>";
		} else
			echo "<script>location.href='?act=category';</script>";
	}
  
	}
elseif ($_GET['act']=="viewcategory") {
    $categoryCode=$_GET['categoryCode'];
    $TPL->newBlock("CURRENTLI");
    $TPL->assign("CURRENTSETTING","active");
    $TPL->assign("CURRENTSETTINGCATEGORY","active");
      
    $TPL->newBlock("HEADER");
    $TPL->assign("HEADERTITLE","Detail");
    $TPL->assign("HEADERDESC","View detail category"); 
    $TPL->assign("HEADERICON","glyphicon-settings"); 
    
    $TPL->newBlock("BREADCRUMB");
    $TPL->assign("BREADCRUMBICON","glyphicon-display");
    $TPL->assign("BREADCRUMBLEVEL1","Setting"); 
    $TPL->assign("BREADCRUMBLINKLEVEL1","?act=main"); 
    $TPL->assign("BREADCRUMBLEVEL2","Category");     
    $TPL->assign("BREADCRUMBACTIVELEVEL2","");
    $TPL->assign("BREADCRUMBLINKLEVEL2","?act=category"); 
    $TPL->assign("BREADCRUMBRIGHTLEVEL2","icon-angle-right"); 
    $TPL->assign("BREADCRUMBLEVEL3","View detail");     
    $TPL->assign("BREADCRUMBACTIVELEVEL3","active");    
     
    $SQL = "SELECT a.categoryId
    ,a.categoryCode
    ,a.categoryName
    ,a.sla
    ,uc.userFullName as createdBy 
    ,a.createdDate
    ,um.userFullName as modifiedBy 
    ,a.modifiedDate
    FROM m_category a 
    inner join m_user uc ON a.createdBy=uc.username 
    inner join m_user um ON a.modifiedBy=um.username
    where a.rowstatus='1' and a.categoryCode='$categoryCode' order by a.categoryName Asc";
    
        $RS = $DB->Execute($SQL);    	
        if($RS AND !$RS->EOF) { 
        $TPL->newblock("VIEWCATEGORY");
        $TPL->assign("CATEGORYID",$RS->fields['categoryId']);
       	$TPL->assign("CATEGORYCODE",$RS->fields['categoryCode']);
       	$TPL->assign("CATEGORYNAME",$RS->fields['categoryName']);
        $TPL->assign("SLA",$RS->fields['sla']);
       	
			}    
		
 } 

   elseif ($_GET['act']=="category") {
    $user=$_SESSION['ses_userName'];
    $TPL->newBlock("CURRENTLI");
    $TPL->assign("CURRENTSETTING","active");
    $TPL->assign("CURRENTSETTINGCATEGORY","active");
    
    $TPL->newBlock("HEADER");
    $TPL->assign("HEADERTITLE","Category");
    $TPL->assign("HEADERDESC","Category List"); 
    $TPL->assign("HEADERICON","glyphicon-file"); 
    
    $TPL->newBlock("BREADCRUMB");
    $TPL->assign("BREADCRUMBICON","glyphicon-display");
    $TPL->assign("BREADCRUMBLEVEL1","Setting"); 
    $TPL->assign("BREADCRUMBLINKLEVEL1","?act=main"); 
    $TPL->assign("BREADCRUMBLEVEL2","Category");     
    $TPL->assign("BREADCRUMBACTIVELEVEL2","active");
    $TPL->assign("BREADCRUMBLINKLEVEL2","?act=category"); 
      $SQL = "SELECT a.categoryId
      ,a.categoryCode
      ,a.categoryName
      ,a.sla
      ,uc.userFullName as createdBy 
      ,a.createdDate
      ,um.userFullName as modifiedBy 
      ,a.modifiedDate
      FROM m_category a 
      inner join m_user uc ON a.createdBy=uc.username 
      inner join m_user um ON a.modifiedBy=um.username
      where a.rowstatus='1' order by a.categoryName Asc";
    $TPL->newBlock("TABLE");
		$RS = $DB->Execute($SQL);
		if($RS AND !$RS->EOF) {
		  $no=0;
			while(!$RS->EOF) {
			  $no++;
				$TPL->newBlock("LIST");
				$TPL->assign("NO",$no);
				$TPL->assign("CATEGORYID",$RS->fields['categoryId']);
				$TPL->assign("CATEGORYCODE",$RS->fields['categoryCode']);
				$TPL->assign("CATEGORYNAME",$RS->fields['categoryName']);
        $TPL->assign("SLA",$RS->fields['sla']);
				$TPL->assign("CREATEDBY",$RS->fields['createdBy']);
        $TPL->assign("CREATEDDATE",date('d M Y h:i:s A', strtotime($RS->fields['createdDate']))); 
				$TPL->assign("MODIFIEDBY",$RS->fields['modifiedBy']);
				$TPL->assign("MODIFIEDDATE",date('d M Y h:i:s A', strtotime($RS->fields['modifiedDate']))); 
				$RS->MoveNext();	
			}
		$RS->Close();
		}    		
	}
 
 	elseif ($_GET['act']=="delpriviliges") {
		$categoryCode=$_GET['categoryCode'];
    $user=$_SESSION['ses_userName'];
		$SQL = "UPDATE m_userCATEGORY SET rowstatus='0',modifiedBy='$user',modifiedDate=GETDATE() WHERE categoryCode='$categoryCode'";
	
		if ($DB->Execute($SQL)==0) {
			echo "<script>alert('Delete Failed !');window.history.back();</script>";
		} else
			echo "<script>window.history.back();</script>";
	}	
 
  	elseif ($_GET['act']=="delcategory") {
		$categoryCode=$_GET['categoryCode'];
    $user=$_SESSION['ses_userName'];
		$SQL = "UPDATE m_category SET rowstatus='0',modifiedBy='$user',modifiedDate=now() WHERE categoryCode='$categoryCode'";
	
		if ($DB->Execute($SQL)==0) {
			echo "<script>alert('Delete Failed !');location.href='?do=category';</script>";
		} else
			echo "<script>location.href='?act=category';</script>";
	}
 
   elseif ($_GET['act']=="seditcategory") {
    $categoryCode=$_GET['categoryCode'];
    $categoryName=$_POST['categoryName'];
    $sla=$_POST['sla'];
		$user=$_SESSION['ses_userName'];


    $SQL="select categoryName from m_category where categoryName='$categoryName' and categoryCode<>'$categoryCode'";
    $RS = $DB->Execute($SQL);
    if($RS AND !$RS->EOF) {
    $categoryNameCheck=$RS->fields['categoryName'];
    $RS->Close();
    } 
          
    if ($categoryNameCheck==$categoryName){
    echo "<script>alert('Category Name $categoryName exist!');location.href='?act=viewcategory&categoryCode=".$categoryCode."';</script>";
    }
    else
    {
      $SQL = "Update m_category set
      categoryName='$categoryName',
      sla='$sla',
      modifiedBy='$user',
      modifiedDate=now() 
      where categoryCode='$categoryCode'";
if ($DB->Execute($SQL)==0) {
echo "<script>alert('Update Failed !');location.href='?act=category';</script>";
} else
echo "<script>location.href='?act=category';</script>";	
}
    }

        
    ?>