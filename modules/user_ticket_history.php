<?php 
	if ($_GET['act']=="viewtickethistory") {
    $ticketno=$_GET['ticketNo'];
    
    $user=$_SESSION['ses_userName'];
    $TPL->newBlock("CURRENTLI");
    $TPL->assign("CURRENTTICKET","active");
    $TPL->assign("CURRENTTICKETHISTORY","active");
    
    $TPL->newBlock("HEADER");
    $TPL->assign("HEADERTITLE","Ticket");
    $TPL->assign("HEADERDESC","History"); 
    $TPL->assign("HEADERICON","glyphicon-history"); 
    
    $TPL->newBlock("BREADCRUMB");
    $TPL->assign("BREADCRUMBICON","glyphicon-display");
    $TPL->assign("BREADCRUMBLEVEL1","Ticket"); 
    $TPL->assign("BREADCRUMBLINKLEVEL1","?act=main"); 
    $TPL->assign("BREADCRUMBLEVEL2","History List");     
    $TPL->assign("BREADCRUMBACTIVELEVEL2","active");
    $TPL->assign("BREADCRUMBLINKLEVEL2","?act=tickethistory"); 
    $TPL->assign("BREADCRUMBLEVEL3","View detail");     
    $TPL->assign("BREADCRUMBACTIVELEVEL3","active");    
     
    $SQL = "SELECT t.ticketId, t.ticketNo, t.title, t.description, t.file, t.createdDate, t.createdBy, t.modifiedBy, t.modifiedDate, 
      t.rowStatus, t.status, u.userFullName,st.statusName,t.categoryCode,c.categoryName,t.requestDate,t.closedDate,t.completedDate,t.filename,t.dueDate,ua.userFullName as analyst
      FROM ".$DB_DEFAULT.".ticket t 
      inner join m_status st ON t.status=st.statuscode
      inner join m_category c ON t.categoryCode=c.categoryCode
      left join m_user ua ON t.analyst=ua.username
      LEFT JOIN ".$DB_DEFAULT.".m_user u ON t.createdBy=u.username
      where t.ticketNo='$ticketno' order by t.ticketNo Desc";
    
		$RS = $DB->Execute($SQL);
		if($RS AND !$RS->EOF) {
				$TPL->newBlock("VIEWTICKETHISTORY");
				$TPL->assign("TICKETID",$RS->fields['ticketId']);
				$TPL->assign("TICKETNO",$RS->fields['ticketNo']);
				$TPL->assign("TITLE",$RS->fields['title']);
        $TPL->assign("DESCRIPTION",$RS->fields['description']);
				$TPL->assign("FILE",$RS->fields['file']);
				$TPL->assign("STATUS",$RS->fields['status']);
        $TPL->assign("CREATEDBY",$RS->fields['userFullName']);
        $TPL->assign("CREATEDDATE",$RS->fields['createdDate']);
				$TPL->assign("MODIFIEDBY",$RS->fields['modifiedBy']);
				$TPL->assign("MODIFIEDDATE",$RS->fields['modifiedDate']);                                           
				$TPL->assign("STATUSNAME",$RS->fields['statusName']);
				$TPL->assign("CATEGORYCODE",$RS->fields['categoryCode']);
        $TPL->assign("CATEGORYNAME",$RS->fields['categoryName']);
        $TPL->assign("ANALYST",$RS->fields['analyst']);
        $TPL->assign("REQUESTDATE",date('d M Y', strtotime($RS->fields['requestDate'])));
        $TPL->assign("CLOSEDDATE",date('d M Y', strtotime($RS->fields['closedDate'])));
        $TPL->assign("COMPLETEDDATE",date('d M Y', strtotime($RS->fields['completedDate'])));
        $TPL->assign("DUEDATE",date('d M Y', strtotime($RS->fields['dueDate'])));
        if ($RS->fields['filename']<>'')
        {
          $TPL->assign("FILENAME","<a href='".$RS->fields['filename']."' target='_blank'><submit class='btn btn-info'>Download <i class='icon-download'></i></submit></a>");
              
        }
        else
        {
          $TPL->assign("FILENAME","<a href='#'>Document not found</a>");
        }
        

          $statuscode=$RS->fields['status'];
          $SQL1 = "SELECT ms.nextStatusCode,s.statusName FROM m_mapping_status ms 
          inner join m_status s on ms.nextStatusCode=s.statusCode where ms.rowStatus='1' and ms.statusCode='$statuscode' order by s.statusName asc";		
          $RS1 = $DB->Execute($SQL1);
          if($RS1 AND !$RS1->EOF) {
          $statusmapp = $statusmapp."<option value=''>Please select</option>";
          $statusmapp= "<option value=".$RS->fields['status']." selected>".$RS->fields['statusName']."</option>";
          while(!$RS1->EOF) {
               $statusmapp = $statusmapp."<option value=".$RS1->fields['nextStatusCode'].">".$RS1->fields['statusName']."</option>";
               $RS1->MoveNext();	
           }
           $RS1->Close();
         }

     $TPL->assign("STATUSMAPP",$statusmapp);

     $SQL2 = "SELECT username,userFullName FROM m_user
     where rowStatus='1' and userTypeId='2' order by userFullName asc";		
     $RS2 = $DB->Execute($SQL2);
     if($RS2 AND !$RS2->EOF) {
     $pelaksana = $pelaksana."<option value=''>Please select</option>";
     while(!$RS2->EOF) {
          $pelaksana = $pelaksana."<option value=".$RS2->fields['username'].">".$RS2->fields['userFullName']."</option>";
          $RS2->MoveNext();	
      }
      $RS2->Close();
    }
    if ($sess_usertypeid=="1"){
      $TPL->newBlock("ASSIGN"); 
    }
    $TPL->assign("PELAKSANA",$pelaksana);

		$RS->Close();
		}
   

		
 } 

   elseif ($_GET['act']=="tickethistory") {
    $user=$_SESSION['ses_userName'];
    $TPL->newBlock("CURRENTLI");
    $TPL->assign("CURRENTTICKET","active");
    $TPL->assign("CURRENTTICKETHISTORY","active");
    
    $TPL->newBlock("HEADER");
    $TPL->assign("HEADERTITLE","Ticket");
    $TPL->assign("HEADERDESC","History"); 
    $TPL->assign("HEADERICON","glyphicon-history"); 
    
    $TPL->newBlock("BREADCRUMB");
    $TPL->assign("BREADCRUMBICON","glyphicon-display");
    $TPL->assign("BREADCRUMBLEVEL1","Ticket"); 
    $TPL->assign("BREADCRUMBLINKLEVEL1","?act=main"); 
    $TPL->assign("BREADCRUMBLEVEL2","History List");     
    $TPL->assign("BREADCRUMBACTIVELEVEL2","active");
    $TPL->assign("BREADCRUMBLINKLEVEL2","?act=tickethistory"); 
    $TPL->newBlock("TABLEHISTORY");
    if ($sess_usertypeid=="1"){
      $condition= "and t.status in ('S02','S03','S04','S06')";
    }
    elseif ($sess_usertypeid=="2")
    {
      $condition= "and t.status in ('S03','S04') and t.analyst='".$user."'";
    }
    elseif ($sess_usertypeid=="3")
    {
      $condition= "and t.status in ('S04','S06','S07') and t.createdBy='".$user."'";
    }
    else
    {
      $condition='';
    }
      $SQL = "SELECT t.ticketId, t.ticketNo, t.title, t.description, t.file, t.createdDate, t.createdBy, t.modifiedBy, t.modifiedDate, 
      t.rowStatus, t.status, u.userFullName,st.statusName,c.categoryName,t.requestDate,t.closedDate,t.completedDate,t.dueDate,ua.userFullName as analyst
      FROM ".$DB_DEFAULT.".ticket t 
      inner join m_status st ON t.status=st.statuscode
      inner join m_category c ON t.categoryCode=c.categoryCode
      left join m_user ua ON t.analyst=ua.username
      LEFT JOIN ".$DB_DEFAULT.".m_user u ON t.createdBy=u.username 
      where t.rowstatus='1' ".$condition." order by t.ticketNo Desc";
    
		$RS = $DB->Execute($SQL);
		if($RS AND !$RS->EOF) {
		  $no=0;
			while(!$RS->EOF) {
			  $no++;
				$TPL->newBlock("LISTHISTORY");
				$TPL->assign("NO",$no);
				$TPL->assign("TICKETID",$RS->fields['ticketId']);
				$TPL->assign("TICKETNO",$RS->fields['ticketNo']);
				$TPL->assign("TITLE",$RS->fields['title']);
        $TPL->assign("DESCRIPTION",$RS->fields['description']);
				$TPL->assign("FILE",$RS->fields['file']);
				$TPL->assign("STATUS",$RS->fields['status']);
        $TPL->assign("CREATEDBY",$RS->fields['userFullName']);
        $TPL->assign("CREATEDDATE",$RS->fields['createdDate']);
				$TPL->assign("MODIFIEDBY",$RS->fields['modifiedBy']);
				$TPL->assign("MODIFIEDDATE",$RS->fields['modifiedDate']);                                           
				$TPL->assign("STATUS",$RS->fields['statusName']);
				$TPL->assign("CATEGORYNAME",$RS->fields['categoryName']);
        $TPL->assign("ANALYST",$RS->fields['analyst']);
        $TPL->assign("REQUESTDATE",date('d M Y', strtotime($RS->fields['requestDate'])));
        $TPL->assign("CLOSEDDATE",date('d M Y', strtotime($RS->fields['closedDate'])));
        $TPL->assign("COMPLETEDDATE",date('d M Y', strtotime($RS->fields['completedDate'])));
        $TPL->assign("DUEDATE",date('d M Y', strtotime($RS->fields['dueDate'])));
				$RS->MoveNext();	
			}
		$RS->Close();
		}    		
	}
 
      
    ?>