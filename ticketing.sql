-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: NB405
-- Generation Time: Jun 25, 2021 at 05:54 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticketing`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_category`
--

CREATE TABLE `m_category` (
  `categoryId` int(11) NOT NULL,
  `categoryCode` char(3) NOT NULL,
  `categoryName` varchar(20) NOT NULL,
  `sla` int(11) NOT NULL DEFAULT 0,
  `createdBy` char(3) NOT NULL,
  `createdDate` datetime NOT NULL,
  `modifiedBy` char(3) NOT NULL,
  `modifiedDate` datetime NOT NULL,
  `rowStatus` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_category`
--

INSERT INTO `m_category` (`categoryId`, `categoryCode`, `categoryName`, `sla`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`, `rowStatus`) VALUES
(1, 'C01', 'Team Support', 3, 'FMJ', '2021-06-18 17:04:31', 'FMJ', '2021-06-20 00:11:34', '1'),
(2, 'C02', 'Billing', 4, 'FMJ', '2021-06-18 17:05:00', 'FMJ', '2021-06-18 17:05:00', '1'),
(3, 'C03', 'Call Center', 3, 'CLI', '2021-06-19 23:58:45', 'FMJ', '2021-06-20 00:10:26', '1'),
(4, 'C04', 'Aplikasi core system', 4, 'FMJ', '2021-06-22 21:22:39', 'FMJ', '2021-06-22 21:22:39', '1');

-- --------------------------------------------------------

--
-- Table structure for table `m_mapping_status`
--

CREATE TABLE `m_mapping_status` (
  `mappingStatusId` int(11) NOT NULL,
  `statusCode` char(3) NOT NULL,
  `nextStatusCode` char(3) NOT NULL,
  `createdBy` char(3) NOT NULL,
  `createdDate` datetime NOT NULL,
  `modifiedBy` char(3) NOT NULL,
  `modifiedDate` datetime NOT NULL,
  `rowStatus` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_mapping_status`
--

INSERT INTO `m_mapping_status` (`mappingStatusId`, `statusCode`, `nextStatusCode`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`, `rowStatus`) VALUES
(1, 'S01', 'S02', 'FMJ', '2021-06-18 18:25:32', 'FMJ', '2021-06-18 18:25:32', '1'),
(2, 'S01', 'S06', 'FMJ', '2021-06-19 07:01:26', 'FMJ', '2021-06-19 07:01:26', '1'),
(3, 'S02', 'S03', 'FMJ', '2021-06-19 07:01:58', 'FMJ', '2021-06-19 07:01:58', '1'),
(4, 'S03', 'S04', 'FMJ', '2021-06-19 07:13:36', 'FMJ', '2021-06-19 07:13:36', '1'),
(5, 'S03', 'S05', 'FMJ', '2021-06-19 07:13:48', 'FMJ', '2021-06-19 07:13:48', '1'),
(6, 'S07', 'S01', 'FMJ', '2021-06-19 07:22:13', 'FMJ', '2021-06-19 07:22:13', '1'),
(7, 'S07', 'S06', 'FMJ', '2021-06-19 07:25:36', 'FMJ', '2021-06-19 07:25:36', '1'),
(15, 'S05', 'S02', 'FMJ', '2021-06-22 16:51:08', 'FMJ', '2021-06-22 16:51:08', '1'),
(16, 'S05', 'S03', 'FMJ', '2021-06-22 16:51:08', 'FMJ', '2021-06-22 16:51:08', '1');

-- --------------------------------------------------------

--
-- Table structure for table `m_status`
--

CREATE TABLE `m_status` (
  `statusId` int(11) NOT NULL,
  `statusCode` char(3) NOT NULL,
  `statusName` varchar(20) NOT NULL,
  `createdBy` char(3) NOT NULL,
  `createdDate` datetime NOT NULL,
  `modifiedBy` char(3) NOT NULL,
  `modifiedDate` datetime NOT NULL,
  `rowStatus` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_status`
--

INSERT INTO `m_status` (`statusId`, `statusCode`, `statusName`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`, `rowStatus`) VALUES
(1, 'S01', 'Requested', 'FMJ', '2021-06-18 16:52:02', 'FMJ', '2021-06-18 16:52:02', '1'),
(2, 'S02', 'On Progress', 'FMJ', '2021-06-18 16:52:02', 'FMJ', '2021-06-18 16:52:02', '1'),
(3, 'S03', 'Closed', 'FMJ', '2021-06-18 16:53:59', 'FMJ', '2021-06-18 16:53:59', '1'),
(4, 'S04', 'Completed', 'FMJ', '2021-06-18 16:54:25', 'FMJ', '2021-06-18 16:54:25', '1'),
(5, 'S05', 'Re-Open', 'FMJ', '2021-06-18 16:55:02', 'FMJ', '2021-06-18 16:55:02', '1'),
(6, 'S06', 'Reject', 'FMJ', '2021-06-18 16:55:22', 'FMJ', '2021-06-18 16:55:22', '1'),
(8, 'S07', 'Draft', 'FMJ', '2021-06-19 07:20:12', 'FMJ', '2021-06-19 07:20:12', '1');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE `m_user` (
  `userId` int(11) NOT NULL,
  `userName` char(3) NOT NULL,
  `userFullName` varchar(100) NOT NULL,
  `password` char(32) NOT NULL,
  `email` varchar(60) NOT NULL,
  `userTypeId` int(11) NOT NULL,
  `createdDate` datetime NOT NULL,
  `createdBy` char(3) NOT NULL,
  `modifiedDate` datetime NOT NULL,
  `modifiedBy` char(3) NOT NULL,
  `rowStatus` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`userId`, `userName`, `userFullName`, `password`, `email`, `userTypeId`, `createdDate`, `createdBy`, `modifiedDate`, `modifiedBy`, `rowStatus`) VALUES
(3, 'FMJ', 'Fahrul Mundjid', '21232f297a57a5a743894a0e4a801fc3', 'fahrul.mundjid@gmail.com', 1, '2021-06-18 15:12:42', 'FMJ', '2021-06-18 15:12:42', 'FMJ', '1'),
(5, 'HAR', 'Haryono', '21232f297a57a5a743894a0e4a801fc3', 'dot.haryono@gmail.com', 2, '2021-06-18 20:43:38', 'FMJ', '2021-06-18 20:43:38', 'FMJ', '1'),
(6, 'CLI', 'PT Aksara Muda Jakarta', '21232f297a57a5a743894a0e4a801fc3', 'arul.cow@gmail.com', 3, '2021-06-19 11:56:13', 'FMJ', '2021-06-19 11:56:13', 'FMJ', '1'),
(7, 'CL2', 'Klient 2', 'e10adc3949ba59abbe56e057f20f883e', 'fl@gmail.com', 3, '2021-06-20 00:33:50', 'FMJ', '2021-06-20 01:05:57', 'CL2', '1'),
(8, 'KL1', 'PT Klient 1', 'fcea920f7412b5da7be0cf42b8c93759', 'kl@gmail.com', 3, '2021-06-22 21:25:12', 'FMJ', '2021-06-22 21:58:12', 'KL1', '1'),
(9, 'FAH', 'Fahrul Mundjid', 'e10adc3949ba59abbe56e057f20f883e', 'fa2@gmail.com', 2, '2021-06-22 21:33:17', 'FMJ', '2021-06-22 21:33:17', 'FMJ', '1');

-- --------------------------------------------------------

--
-- Table structure for table `m_user_type`
--

CREATE TABLE `m_user_type` (
  `userTypeId` int(11) NOT NULL,
  `userTypeName` varchar(60) NOT NULL,
  `createdBy` char(3) NOT NULL,
  `createdDate` datetime NOT NULL,
  `modifiedBy` char(3) NOT NULL,
  `modifiedDate` datetime NOT NULL,
  `rowStatus` char(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_user_type`
--

INSERT INTO `m_user_type` (`userTypeId`, `userTypeName`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`, `rowStatus`) VALUES
(1, 'Admin', 'FMJ', '2021-06-18 15:54:42', 'FMJ', '2021-06-18 15:54:42', '1'),
(2, 'Pelaksana', 'FMJ', '2021-06-18 15:54:42', 'FMJ', '2021-06-18 15:54:42', '1'),
(3, 'Client / Partner', 'FMJ', '2021-06-18 15:55:25', 'FMJ', '2021-06-18 15:55:25', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `ticketId` int(11) NOT NULL,
  `ticketNo` char(10) NOT NULL,
  `title` varchar(120) NOT NULL,
  `description` text NOT NULL,
  `file` varchar(120) DEFAULT NULL,
  `createdDate` datetime NOT NULL,
  `createdBy` char(3) NOT NULL,
  `modifiedBy` char(3) NOT NULL,
  `modifiedDate` datetime NOT NULL,
  `rowStatus` char(1) NOT NULL,
  `status` char(3) NOT NULL,
  `categoryCode` char(3) NOT NULL,
  `requestDate` datetime DEFAULT NULL,
  `dueDate` date DEFAULT NULL,
  `ClosedDate` datetime DEFAULT NULL,
  `CompletedDate` datetime DEFAULT NULL,
  `analyst` char(3) NOT NULL,
  `filename` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`ticketId`, `ticketNo`, `title`, `description`, `file`, `createdDate`, `createdBy`, `modifiedBy`, `modifiedDate`, `rowStatus`, `status`, `categoryCode`, `requestDate`, `dueDate`, `ClosedDate`, `CompletedDate`, `analyst`, `filename`) VALUES
(10, 'T210619001', 'Permintaan perbaikan aplikasi tciketing', 'Fahrul Mundjid comment on 19 Jun 2021 05:33:02 PM&#013;================================================&#013;&#013; &#013;Fahrul Mundjid comment on 19 Jun 2021 08:14:45 AM\r\n================================================\r\nOk Terima kasih\r\n \r\nFahrul Mundjid comment on 19 Jun 2021 07:07:57 AM\r\n================================================\r\nSilahkan di cari\r\n \r\nFahrul Mundjid request on \r\n================================================\r\nDear Team Support,\r\nMohon bantuannya untuk perbaikan aplikasi helpdesk ticketing.\r\nSaat ini sudah tidak bisa berjalan selama 4 hari sehingga menyebabkan pendokumentasian permintaan/keluhan tidak dilakukan.\r\nDitunggu responsenya segera.\r\n\r\nTerima kasih', NULL, '2021-06-19 11:34:18', 'CLI', 'FMJ', '2021-06-19 22:33:02', '1', 'S04', 'C01', '2021-06-02 22:27:33', '2021-06-02', '2021-06-02 22:28:34', '2021-06-01 22:28:37', 'HAR', NULL),
(11, 'T210619002', 'Perbaikan sistem ticketing', 'PT Aksara Muda Jakarta comment on 19 Jun 2021 02:56:20 PM&#013;================================================&#013;Terima kasih&#013; &#013;Haryono comment on 19 Jun 2021 02:55:19 PM\r\n================================================\r\nClosed\r\n \r\nHaryono comment on 19 Jun 2021 02:47:16 PM\r\n================================================\r\ntezzzz\r\n \r\nFahrul Mundjid comment on 19 Jun 2021 02:46:09 PM\r\n================================================\r\nkirim ke analyst\r\n \r\nFahrul Mundjid comment on 19 Jun 2021 06:35:53 AM\r\n================================================\r\nDear Haryono,\r\nMohon dibantu pengecekan case ini ya.\r\n \r\nFahrul Mundjid request on 19 Jun 2021 06:35:18 AM\r\n================================================\r\nDear Team Support,\r\nMohon bantuannya untuk perbaikan aplikasi helpdesk ticketing.\r\nSaat ini sudah tidak bisa berjalan selama 4 hari sehingga menyebabkan pendokumentasian permintaan/keluhan tidak dilakukan.\r\nDitunggu responsenya segera.\r\n\r\nTerima kasih', NULL, '2021-06-19 11:35:18', 'CLI', 'CLI', '2021-06-19 19:56:20', '1', 'S03', 'C01', '0000-00-00 00:00:00', '2021-06-10', '2021-06-19 00:00:00', '2021-06-19 00:00:00', 'HAR', NULL),
(12, 'T210619003', 'Perbaikan aplikasi HRD', 'Haryono comment on 19 Jun 2021 02:43:13 PM&#013;================================================&#013;Sudah ok&#013; &#013;Haryono comment on 19 Jun 2021 08:18:05 AM\r\n================================================\r\nasasas\r\n \r\nFahrul Mundjid comment on 19 Jun 2021 06:46:21 AM\r\n================================================\r\nHalo Har,\r\nMohon dibantu proses ya\r\nThanks & Regards\r\n \r\nFahrul Mundjid request on 19 Jun 2021 06:45:50 AM\r\n================================================\r\nDear All, Mohon bantuannya untuk perbaikan aplikasi HRD ya.\r\n Thanks', NULL, '2021-06-19 11:45:50', 'FMJ', 'HAR', '2021-06-19 19:43:13', '1', 'S02', 'C01', '2021-06-19 11:45:50', '2021-06-01', NULL, NULL, 'HAR', NULL),
(13, 'T210619004', 'Permintaan billing bulan mei', 'PT Aksara Muda Jakarta comment on 19 Jun 2021 07:14:52 AM&#013;================================================&#013;Terima kasih atas bantuannya&#013; &#013;Haryono comment on 19 Jun 2021 07:11:18 AM\r\n================================================\r\nSudah dibuatkan laporannya juga.\r\ndiemail ke alamat abc@gmail.cooom\r\n \r\nFahrul Mundjid comment on 19 Jun 2021 06:59:09 AM\r\n================================================\r\nDear Haryono, \r\nMohon dibantu perihal ini ya.\r\nDibuatkan sekalian duplikasinya.\r\nTerima kasih\r\n \r\nPT Aksara Muda Jakarta request on 19 Jun 2021 06:57:44 AM\r\n================================================\r\nDear Tim,\r\nMohon bantuannya dibuatkan billing gabungan dibulan mei 2021 ya.\r\nTerima kasih', NULL, '2021-06-19 11:57:44', 'CLI', 'CLI', '2021-06-19 12:14:52', '1', 'S04', 'C02', '2021-06-19 11:57:44', NULL, NULL, NULL, '', NULL),
(14, 'T210619005', 'Permintaan draft ticket', 'PT Aksara Muda Jakarta comment on 19 Jun 2021 07:34:40 AM&#013;================================================&#013;Batal permintaan&#013; &#013;PT Aksara Muda Jakarta comment on 19 Jun 2021 07:34:27 AM\r\n================================================\r\nSaya akan tunggu ya, sampai datang\r\n \r\nPT Aksara Muda Jakarta comment on 19 Jun 2021 07:33:27 AM\r\n================================================\r\nasasa\r\n \r\nPT Aksara Muda Jakarta comment on 19 Jun 2021 07:23:29 AM\r\n================================================\r\nKelangkapan 1\r\n \r\nPT Aksara Muda Jakarta request on 19 Jun 2021 07:20:56 AM\r\n================================================\r\nPermintaan draft ticket', NULL, '2021-06-19 12:20:56', 'CLI', 'CLI', '2021-06-19 12:34:40', '1', 'S06', 'C02', '2021-06-19 12:20:56', NULL, NULL, NULL, '', NULL),
(15, 'T210619006', 'AASS', 'Fahrul Mundjid request on 19 Jun 2021 03:51:37 PM&#013;================================================&#013;ASASASAS', NULL, '2021-06-19 20:51:37', 'FMJ', 'FMJ', '2021-06-19 20:51:37', '1', 'S01', 'C02', '2021-06-19 20:51:37', NULL, NULL, NULL, '', NULL),
(16, 'T210619007', 'rtrtrtrt', 'Fahrul Mundjid request on 19 Jun 2021 03:53:20 PM&#013;================================================&#013;rtrtrtrtrt', NULL, '2021-06-19 20:53:20', 'FMJ', 'FMJ', '2021-06-19 20:53:20', '1', 'S01', 'C01', '2021-06-19 20:53:20', NULL, NULL, NULL, '', NULL),
(17, 'T210619008', 'asasa', 'Fahrul Mundjid request on 19 Jun 2021 03:56:41 PM&#013;================================================&#013;asasasasasasas', NULL, '2021-06-19 20:56:41', 'FMJ', 'FMJ', '2021-06-19 20:56:41', '1', 'S01', 'C02', '2021-06-19 20:56:41', NULL, NULL, NULL, '', NULL),
(18, 'T210619009', 'asasa', 'Fahrul Mundjid request on 19 Jun 2021 03:56:53 PM&#013;================================================&#013;asasasasasasas', NULL, '2021-06-19 20:56:53', 'FMJ', 'FMJ', '2021-06-19 20:56:53', '1', 'S01', 'C02', '2021-06-19 20:56:53', NULL, NULL, NULL, '', NULL),
(19, 'T210619010', 'asasa', 'Fahrul Mundjid request on 19 Jun 2021 03:57:43 PM&#013;================================================&#013;asasasasasasas', NULL, '2021-06-19 20:57:43', 'FMJ', 'FMJ', '2021-06-19 20:57:43', '1', 'S01', 'C02', '2021-06-19 20:57:43', NULL, NULL, NULL, '', NULL),
(20, 'T210619011', 'asasa', 'Fahrul Mundjid request on 19 Jun 2021 03:59:14 PM&#013;================================================&#013;asasasasasasas', NULL, '2021-06-19 20:59:14', 'FMJ', 'FMJ', '2021-06-19 20:59:14', '1', 'S01', 'C02', '2021-06-19 20:59:14', NULL, NULL, NULL, '', NULL),
(21, 'T210619012', 'asasasa', 'Fahrul Mundjid request on 19 Jun 2021 03:59:25 PM&#013;================================================&#013;asasa', NULL, '2021-06-19 20:59:25', 'FMJ', 'FMJ', '2021-06-19 20:59:25', '1', 'S01', 'C02', '2021-06-19 20:59:25', NULL, NULL, NULL, '', NULL),
(22, 'T210619013', 'qewqwqew', 'Fahrul Mundjid request on 19 Jun 2021 04:02:30 PM&#013;================================================&#013;qweweqwe', NULL, '2021-06-19 21:02:30', 'FMJ', 'FMJ', '2021-06-19 21:02:30', '1', 'S01', 'C02', '2021-06-19 21:02:30', NULL, NULL, NULL, '', NULL),
(23, 'T210619014', 'aSADWQ', 'Fahrul Mundjid request on 19 Jun 2021 04:03:19 PM&#013;================================================&#013;QWEWQEWEWE', NULL, '2021-06-19 21:03:19', 'FMJ', 'FMJ', '2021-06-19 21:03:19', '1', 'S01', 'C01', '2021-06-19 21:03:19', NULL, NULL, NULL, '', NULL),
(24, 'T210619015', 'SDSD', 'Fahrul Mundjid request on 19 Jun 2021 04:03:51 PM&#013;================================================&#013;SDSD', NULL, '2021-06-19 21:03:51', 'FMJ', 'FMJ', '2021-06-19 21:03:51', '1', 'S01', 'C02', '2021-06-19 21:03:51', NULL, NULL, NULL, '', NULL),
(25, 'T210619016', 'asdsadsad', 'PT Aksara Muda Jakarta comment on 19 Jun 2021 06:08:08 PM&#013;================================================&#013;Ok terima kasih&#013; &#013;Haryono comment on 19 Jun 2021 06:04:07 PM\r\n================================================\r\nSilahkan cek kemabia\r\n \r\nHaryono comment on 19 Jun 2021 06:03:50 PM\r\n================================================\r\nSedang dilakukan investigas\r\n \r\nFahrul Mundjid comment on 19 Jun 2021 05:39:18 PM\r\n================================================\r\nSilahkan dibantu\r\n \r\nFahrul Mundjid comment on 19 Jun 2021 05:39:00 PM\r\n================================================\r\nMasih mengunngu ya\r\n \r\nFahrul Mundjid request on 19 Jun 2021 04:06:48 PM\r\n================================================\r\nadsadsadsdsad', NULL, '2021-06-19 21:06:48', 'CLI', 'CLI', '2021-06-19 23:08:08', '1', 'S04', 'C02', '2021-06-19 21:06:48', '2021-06-24', '2021-06-19 23:04:07', '2021-06-19 23:08:08', 'HAR', NULL),
(26, 'T210619017', 'asas', 'Haryono comment on 19 Jun 2021 05:35:29 PM&#013;================================================&#013;ok&#013; &#013;Fahrul Mundjid comment on 19 Jun 2021 05:34:12 PM\r\n================================================\r\nzXzXZXZXZX\r\n \r\nFahrul Mundjid request on 19 Jun 2021 04:08:09 PM\r\n================================================\r\nasasasasas', NULL, '2021-06-19 21:08:09', 'FMJ', 'HAR', '2021-06-19 22:35:29', '1', 'S02', 'C02', '2021-06-19 21:08:09', '2021-06-24', NULL, NULL, '', NULL),
(27, 'T210619018', 'asasasa', 'Haryono comment on 19 Jun 2021 05:35:17 PM&#013;================================================&#013;Sedangmenunggu dokument pelangkap&#013; &#013;Fahrul Mundjid comment on 19 Jun 2021 05:34:01 PM\r\n================================================\r\nzcxcxcxcxc\r\n \r\nFahrul Mundjid request on 19 Jun 2021 04:08:50 PM\r\n================================================\r\naasas', NULL, '2021-06-19 21:08:50', 'FMJ', 'HAR', '2021-06-19 22:35:17', '1', 'S02', 'C02', '2021-06-19 21:08:50', '2021-06-24', NULL, NULL, '', NULL),
(28, 'T210619019', 'asasasasas', 'Haryono comment on 19 Jun 2021 05:34:57 PM&#013;================================================&#013;Sudah dibuatkan&#013; &#013;Fahrul Mundjid comment on 19 Jun 2021 05:33:51 PM\r\n================================================\r\nsdsadadsd\r\n \r\nFahrul Mundjid request on 19 Jun 2021 04:20:51 PM\r\n================================================\r\nasasasasasas', NULL, '2021-06-19 21:20:51', 'FMJ', 'HAR', '2021-06-19 22:34:57', '1', 'S03', 'C02', '2021-06-19 21:20:51', '2021-06-24', '2021-06-19 22:34:57', NULL, '', 'uploads/T210619019/529777_3073844610522_1689813079_n (1).jpg'),
(29, 'T210619020', 'Permintaan perubahan aplikasi', 'PT Aksara Muda Jakarta comment on 19 Jun 2021 06:17:14 PM&#013;================================================&#013;Dibatalkan&#013; &#013;PT Aksara Muda Jakarta request on 19 Jun 2021 06:09:17 PM\r\n================================================\r\nPermintaan perubahan aplikasi.\r\nPermintaan perubahan aplikasi.\r\nPermintaan perubahan aplikasi.', NULL, '2021-06-19 23:09:17', 'CLI', 'CLI', '2021-06-19 23:17:14', '1', 'S06', 'C01', '2021-06-19 23:09:17', NULL, NULL, NULL, '', 'uploads/T210619020/4967010.jpg'),
(30, 'T210619021', 'Permintaan ABC', 'PT Aksara Muda Jakarta request on 19 Jun 2021 06:20:22 PM&#013;================================================&#013;asasasasasa', NULL, '2021-06-19 23:20:22', 'CLI', 'CLI', '2021-06-19 23:20:22', '1', 'S07', 'C02', '2021-06-19 23:20:22', NULL, NULL, NULL, '', 'uploads/T210619021/'),
(31, 'T210622001', 'Portal Partner SQL Server Monitoring Problem', 'PT Klient 1 comment on 22 Jun 2021 04:53:43 PM&#013;================================================&#013;Dear PT,\r\nTerima kasih&#013; &#013;Fahrul Mundjid comment on 22 Jun 2021 04:52:53 PM\r\n================================================\r\nDear Klient,\r\nIni sudah ok, tlng dicek kembali\r\nTerima kasih\r\n \r\nPT Klient 1 comment on 22 Jun 2021 04:45:56 PM\r\n================================================\r\nDear Team,\r\nMasih belum bisa.\r\nMohon dibantu.\r\nTerima kasih\r\n \r\nFahrul Mundjid comment on 22 Jun 2021 04:41:41 PM\r\n================================================\r\nDear Klien,\r\nSudah dilakukan pengecekan dan sudah bisa digunakan kembali.\r\n\r\nTerima kasih\r\n \r\nFahrul Mundjid comment on 22 Jun 2021 04:39:01 PM\r\n================================================\r\nDear Mas Fahrul,\r\nMohon dibantu ditindaklanjuti perihal ini.\r\nTerima kasih\r\n \r\nPT Klient 1 comment on 22 Jun 2021 04:29:20 PM\r\n================================================\r\nDokument tidak perlu\r\n \r\nPT Klient 1 request on 22 Jun 2021 04:27:47 PM\r\n================================================\r\nDear Team Exellent Solutiondo,\r\nPortal Partner SQL Server Monitoring Problem.\r\nMoho bantuannya pengecekannya', NULL, '2021-06-22 21:27:47', 'KL1', 'KL1', '2021-06-22 21:53:43', '1', 'S03', 'C01', '2021-06-22 21:27:47', '2021-06-25', '2021-06-22 21:52:53', '2021-06-22 21:53:43', 'FAH', 'uploads/T210622001/');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_category`
--
ALTER TABLE `m_category`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `m_mapping_status`
--
ALTER TABLE `m_mapping_status`
  ADD PRIMARY KEY (`mappingStatusId`);

--
-- Indexes for table `m_status`
--
ALTER TABLE `m_status`
  ADD PRIMARY KEY (`statusId`);

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `userName_UNIQUE` (`userName`);

--
-- Indexes for table `m_user_type`
--
ALTER TABLE `m_user_type`
  ADD PRIMARY KEY (`userTypeId`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`ticketId`),
  ADD UNIQUE KEY `ticketNo_UNIQUE` (`ticketNo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_category`
--
ALTER TABLE `m_category`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_mapping_status`
--
ALTER TABLE `m_mapping_status`
  MODIFY `mappingStatusId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `m_status`
--
ALTER TABLE `m_status`
  MODIFY `statusId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `m_user_type`
--
ALTER TABLE `m_user_type`
  MODIFY `userTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `ticketId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
